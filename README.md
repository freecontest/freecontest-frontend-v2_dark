#Frontend v2 dark theme

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
npm run sass (hot-reload for sass)
```

### Compiles and minifies for production
```
npm run build
```