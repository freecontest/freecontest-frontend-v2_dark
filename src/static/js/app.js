import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Router, Link, Route, Switch, Redirect } from 'react-router-dom'
// import Register from '../components/register'
import LogIn from '../components/login'
import Register from '../components/register'
import { App as Home } from '../components/home'
import Post from '../components/post'
import Contest from '../components/contest'
import User from '../components/user'
import ChangeUserSettings from '../components/changeUserSettings'
import AutoLogin from '../components/autologin'
import HasReg from '../components/hasreg'
import RegisterContest from '../components/registerContest'

import {FreeContest as FC, history} from '../js/action'

class App extends Component{

    constructor(props){
        super(props)
        this.state = {
            logined: null,
            user: null,
            noticeTime: null,
            intervalVar: null,
            notificationEnabled: false,
        }

        this.setReminder = this.setReminder.bind(this)
        this.check = this.check.bind(this)
        this.noticeMe = this.noticeMe.bind(this)
    }

    componentDidMount(){
        FC.getContest((res) => {
            let upcoming = []
            res.forEach(c => {
                let startedTime = (new Date(c.startTime)).getTime()
                let currentTime = new Date().getTime()

                if(startedTime > currentTime){
                    upcoming.push(c)
                }
            })

            if(upcoming.length > 0){
                // console.log('upcoming' ,upcoming)
                this.state.noticeTime = (new Date(upcoming[0].startTime)).getTime()
                this.setReminder(this.state.noticeTime)
            }
        })

        if(window.Notification){
            Notification.requestPermission((res) => {
                if(res === 'granted'){
                    this.state.notificationEnabled = true
                }
            })
        }
    }

    setReminder(time){
        // console.log('reminder set: ' + time)
        this.state.intervalVar = setInterval(this.check, 1000) 
    }

    check(){
        let currentTime = new Date().getTime()
        
        // console.log(currentTime, this.state.noticeTime)

        if(currentTime > this.state.noticeTime){
            this.noticeMe()
            location.reload()
            clearInterval(this.intervalVar)
        }
    }

    noticeMe(){
        if(this.state.notificationEnabled){
            var notification = new Notification("Free Contest", {
                body: 'Kỳ thi đã bắt đầu, hãy tham gia ngay!!!',
                icon: 'https://freecontest.xyz/static/img/favicon.png'
            });
            setTimeout(() => {notification.close()}, 5000)
        }
    }

    render(){
        return(
            <div>
                <Router history={history}>
                    <div>
                        <Switch>
                            <Route path="/" exact={true} component={Home}/>
                            <Route path="/login" component={LogIn}/>
                            <Route path="/registered/:cid" component={HasReg}/>
                            <Route path="/register/new/:cid" component={RegisterContest}/>
                            <Route path="/register" component={Register}/>
                            {/* <Route path="/rating" component={Ranking}/> */}
                            <Route path="/contest" component={Contest}/>
                            <Route path="/user/:uid" component={User}/>
                            <Route path="/settings" component={ChangeUserSettings}/>
                            <Route path="/post/:pid" component={Post}/>
                            <Route path="/autologin/:cid" component={AutoLogin}/>
                            <Route render={() => (
                                <Redirect to="/"/>
                            )}/>
                        </Switch>
                    </div>
                </Router>
            </div>
        )
    }
}

ReactDOM.render(<App/>, document.getElementById('app'))