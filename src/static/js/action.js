import { createBrowserHistory } from 'history'
import iziToast from 'izitoast'
const history = createBrowserHistory()

//const host = '/'
const host = 'https://freecontest.xyz/'

const _fetch = {
    get: async (url) => {
        return await fetch(host + url, { method: 'GET', credentials: 'include' })
        .then(res => res.json())
    },

    post: async (url, data) => {
        return await fetch(host + url, {
            method: 'POST',
            credentials: 'include',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(res => res.json())
    },

    put: async (url, data) => {
        return await fetch(host + url, {
            method: 'PUT',
            credentials: 'include',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(res => res.json())
    },

    patch: async (url, data) => {
        return await fetch(host + url, {
            method: 'PATCH',
            credentials: 'include',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(res => res.json())
    },

    delete: async (url) => {
        return await fetch(host + url, { method: 'DELETE', credentials: 'include' })
        .then(res => res.json())
    }
}

const FreeContest = {
    getSession: (callback) => {
        _fetch.get('api/auth')
        .then((response) => {
            callback(response);
        });
    },

    signIn: (username, password, callback) => {
        _fetch.post('api/auth', {username: username, password: password})
        .then(response => {
            callback(response);
        });
    },

    register: (name, schoolName, yob, username, password, callback) => {
        _fetch.put('api/auth', {
            name: name,
            schoolName: schoolName,
            yob: yob,
            username: username,
            password: password
        })
        .then((response) => {
            callback(response);
        });
    },

    updateWithOutP: (name, schoolName, yob, showOnRanking, callback) => {
        _fetch.put('api/users', {
            name: name,
            schoolName: schoolName,
            yob: yob,
            showOnRanking: showOnRanking
        })
        .then((response) => {
            callback(response);
        });
    },

    updateWithP: (name, schoolName, yob, password, showOnRanking, callback) => {
        _fetch.put('api/users', {
            name: name,
            schoolName: schoolName,
            yob: yob,
            password: password,
            showOnRanking: showOnRanking
        })
        .then((response) => {
            callback(response);
        });
    },

    updateAvt: (email, callback) => {
        _fetch.post('api/users', {
            email: email
        })
        .then((response) => {
            callback(response);
        });
    },

    getPosts: (callback) => {
        _fetch.get('api/posts')
        .then((response) => {
            callback(response);
        });
    },

    getPostWId: (pid, callback) => {
        _fetch.get(`api/posts?postid=${pid}`)
        .then((response) => {
            callback(response);
        });
    },

    createPost: (title, content, callback) => {
        _fetch.post('api/posts', {
            content: content,
            title: title
        })
        .then((response) => {
            callback(response);
        });
    },

    updatePost: (pid, title, content, callback) => {
        _fetch.put('api/posts', {
            pid: pid,
            content: content,
            title: title
        })
        .then((response) => {
            callback(response);
        });
    },

    deletePost: (pid, callback) => {
        _fetch.patch('api/posts', {
            pid: pid
        })
        .then((response) => {
            callback(response);
        });
    },

    getUser: (username, callback) => {
        _fetch.get(`api/users?username=${username}`)
        .then((response) => {
            callback(response);
        });
    },

    getUsers: (callback, limit = 99999, offset = 0) => {
        _fetch.get(`api/users?limit=${limit}&offset=${offset}`)
        .then((response) => {
            callback(response);
        });
    },

    getContest: (callback, limit = 99999) => {
        _fetch.get(`api/contest?limit=${limit}`)
        .then((response) => {
            callback(response);
        });
    },

    getContestX: (uid, callback, limit = 99999) => {
        _fetch.get(`api/contest?uid=${uid}&limit=${limit}`)
        .then((response) => {
            callback(response);
        });
    },

    getContestWID: (id, callback) => {
        _fetch.get(`api/contest?cid=${id}`)
        .then((response) => {
            callback(response);
        });
    },

    updateContest: (cid, name, startTime, endTime, duration, writters, id_contest, code, practice_url, facebook_url, callback) => {
        _fetch.patch('api/contest', {
            cid: cid,
            name: name,
            startTime: startTime,
            endTime: endTime,
            duration: duration,
            writters: writters,
            id_contest: id_contest,
            code: code,
            practice_url: practice_url,
            facebook_url, facebook_url
        })
        .then((response) => {
            callback(response);
        });
    },

    addContest: (name, startTime, endTime, duration, writters, callback) => {
        _fetch.put('api/contest', {
            name: name,
            startTime: startTime,
            endTime: endTime,
            duration: duration,
            writters: writters
        })
        .then((response) => {
            callback(response);
        });
    },

    deleteContest: (cid, callback) => {
        _fetch.post('api/contest', { cid: cid })
        
        .then((response) => {
            callback(response);
        });
    },

    registerContest: (cid, callback) => {
        _fetch.put('api/contest/register', { cid: cid })
        
        .then((response) => {
            callback(response);
        });
    },

    getUsersRegistered: (cid, callback) => {
        _fetch.post('api/contest/register', { cid: cid })
        
        .then((response) => {
            callback(response);
        });
    },

    getTmpPassword: (uid, cid, callback) => {
        _fetch.post('api/tmpPassword', { uid: uid, cid: cid })
        .then((response) => {
            callback(response)
        })
    },

    getSingleContest: (params, callback) => {
        _fetch.post('api/singleContest', params).then((res) => {
            callback(res);
        });
    },

    regSingleContest: (params, callback) => {
        _fetch.post('api/regSingleContest', params).then((res) => {
            callback(res);
        });
    },

    logOut: (callback) => {
        _fetch.delete('api/auth')
        .then((response) => {
            callback(response);
        });
    }
}

const toast = {
    success: (title, message) => {
        iziToast.success({
            title: title,
            message: message,
            // theme: 'dark',
            color: 'green',
            position: 'bottomRight',
            icon: 'fas fa-check-circle',
            timeout: 3000
        })
    },
    error: (title, message) => {
        iziToast.error({
            title: title,
            message: message,
            // theme: 'dark',
            color: 'red',
            position: 'bottomRight',
            icon: 'fas fa-exclamation-triangle',
            timeout: 3000
        })
    }
}

module.exports = { FreeContest, history, toast }