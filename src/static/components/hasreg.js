import React, { Component } from 'react'
import { FreeContest as FC, history, toast } from '../js/action'
import Init from '../js/init'
import moment from 'moment'
import Navbar from '../components/navbar'
import Loader from '../components/loader'


class App extends Component{

    constructor(props){
        super(props)
        this.state = {
            users: null
        }
    }

    componentDidMount(){
        document.title = 'Những người đã đăng kí'

        moment.locale('vi')

        let cid = this.props.match.params.cid
        console.log(cid)

        FC.getUsersRegistered(cid, (res) => {
            if(res.err != undefined){
                toast.error('Thông báo', 'Đã có lỗi xảy ra')
                history.push({ pathname: '/' })
            }

            for(var i = 0; i < res.length; i++){
                res[i].num = i + 1

                if(res[i].color) { res[i].color = 12 }
            }

            this.setState({ users: res })
        })
    }

    render(){
        const users = this.state.users
        return(
            <div>
                <Navbar/>
                {users == null ? (
                    <div className="center" style={{width: '400px', height: '400px'}}>
                        <Loader/>
                    </div>
                ):(
                    <div className="hasreg">
                        <h3>Những người đã đăng kí</h3>
                        <div className="panel panel-default">
                            <div className="table-responsive-md">
                                <table className="table-dark">
                                    <thead>
                                        <tr>
                                            <th className="text-center">#</th>
                                            <th className="text-center">User</th>
                                            <th className="text-center">Đã đăng kí vào lúc</th>
                                        </tr>
                                    </thead>
                                    {users.length > 0 ? (
                                        <tbody>
                                            {users.map(u => (
                                                <tr key={u.num}>
                                                    <td className="text-center">{u.num}</td>
                                                    <td className="text-center"><a href={ "user/" + u.username }><p className={Init.colors[u.color]}>{u.username}</p></a></td>
                                                    <td className="text-center">{moment(u.createdAt).from(moment())}</td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    ):(
                                        <tbody>
                                            <tr>
                                                <td className="text-center" colSpan="3">Chưa có user đăng kí</td>
                                            </tr>
                                        </tbody>
                                    )}
                                </table>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        )
    }
}

module.exports = App