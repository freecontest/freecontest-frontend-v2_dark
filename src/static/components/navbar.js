import React, { Component } from 'react'
import { FreeContest as FC, history, toast } from '../js/action'
import Cookie from 'js-cookie'


class App extends Component{

    constructor(props){
        super(props)
        this.state = {
            toggle: false,
            profileToggle: false,
            logined: false,
            user: {
                name: null,
                username: null
            }
        }

        this.toggle = this.toggle.bind(this)
        this.profileToggle = this.profileToggle.bind(this)
        this.logOut = this.logOut.bind(this)
    }

    toggle(){
        if(this.state.toggle){
            document.getElementsByClassName('nav')[0].classList.remove('open')
        }
        else{
            document.getElementsByClassName('nav')[0].classList.add('open')
        }
        
        this.setState({ toggle: !this.state.toggle })
    }

    profileToggle(){
        if(!this.state.profileToggle){
            document.getElementsByClassName('profile')[0].classList.add('toggle')
        }
        else{
            document.getElementsByClassName('profile')[0].classList.remove('toggle')
        }

        this.setState({ profileToggle: !this.state.profileToggle })
    }

    logOut(){
        
        FC.logOut((res) => {
            if(res.err !== undefined){
                toast.error('Thông báo', 'Đã có lỗi xảy ra')
            }
            else{
                toast.success('Thông báo', 'Đăng xuất thành công')
                if(location.pathname == '/') { window.location.reload() }
                else { history.push('/') }
            }
        })
    }

    componentDidMount(){
        const url = window.location.href.split('/')[3]
        // console.log(url)
        switch(url){
            case 'contest': { document.getElementsByClassName('contest')[0].classList.add('active') }
            break;
            case 'login': { document.getElementsByClassName('login-nav')[0].classList.add('active') }
            break;
            case 'register': { document.getElementsByClassName('register-nav')[0].classList.add('active') }
            break;
            case 'user': {}
            break;
            case 'post': {}
            break;
            case 'registered': {}
            break;
            case 'settings': {}
            break;
            default: { document.getElementsByClassName('home')[0].classList.add('active') }
            break;
        }

        FC.getSession((res) => {
            // console.log(res)
            this.setState({ logined: res.logined, user: res })
        })
    }

    changeTheme(){
        Cookie.remove('dark_theme')
        window.location.reload()
    }

    render(){
        return(
            <div className="navbar">
                <a href="#" className="icon">
                    <div>
                        <svg viewBox="0 0 600 600">
                            <path d="M302.59,40.38,76.17,172.58,77,435.32l225.6,131.37L529,435.32V171.75Zm0,498.87L100.39,422.13l.13-237.08L302.59,67l202.2,118.06.68,237Z"/>
                            <path d="M408.89,347.31a28.36,28.36,0,0,0-22.61-5.74l-39-80.81a55.58,55.58,0,0,0,24.33-46.19c0-30.54-24.17-55.29-54-55.29s-54,24.75-54,55.29c0,18.83,8.69,35.33,22.73,45.31L235.43,365.07a39.76,39.76,0,0,0-9.94-1.26c-22.41,0-40.57,18.62-40.57,41.57S203.08,447,225.49,447s40.58-18.62,40.58-41.58a41.84,41.84,0,0,0-15.36-32.55l50.37-105.62a52.89,52.89,0,0,0,32.38.22l38.71,81a28.84,28.84,0,0,0-4,4.33,30.2,30.2,0,0,0,5.3,41.69A28.54,28.54,0,0,0,414.19,389,30.2,30.2,0,0,0,408.89,347.31Z"/>
                        </svg>
                        <div>
                            Free<b>Contest</b>
                        </div>
                    </div>
                    
                </a>
                    <button className="btn btn-primary toggle" onClick={() => this.toggle()}><i className="fas fa-bars"></i></button>
                <div className="nav-content">
                    <nav className="nav nav-masthead justify-content-center">
                        <ul className="nav-selections">
                            <li className="nav-link"><a className="nav-link-content home" href="/">Trang chủ</a></li>
                            <li className="nav-link"><a className="nav-link-content contest" href="contest">Contest</a></li>
                            <li className="nav-link"><a className="nav-link-content" href="https://www.facebook.com/kc97blf">FreeContest trên Facebook</a></li>
                            <li className="nav-link"><a className="nav-link-content info" href="https://info.freecontest.xyz/">
                                    <div>
                                        <label>New</label>
                                        FreeContest Info
                                    </div>
                                </a>
                            </li>
                            {/* {this.props.user == null ? (
                                <li className="user">
                                    <a className="nav-link" href="login">Đăng nhập</a>
                                    <a className="nav-link" href="register">Đăng kí</a>
                                </li>
                            ):(
                                <li className="user profile">
                                    <a className="nav-link" href="#">Lê Đình Hải(pythagore1123)</a>
                                    <i className="fas fa-angle-right"></i>
                                    <ul>
                                        <li className="one"><a href="#">Trang cá nhân</a></li>
                                        <li className="two"><a href="#">Thay đổi thông tin</a></li>
                                        <li className="three"><a href="#">Đăng xuất</a></li>
                                    </ul>
                                </li> 
                            )} */}
                            <li className="nav-link" style={{display: !this.state.logined ? 'block' : 'none'}}><a className="nav-link-content login-nav" href="login">Đăng nhập</a></li>
                            <li className="nav-link" style={{display: !this.state.logined ? 'block' : 'none'}}><a className="nav-link-content register-nav" href="register">Đăng kí</a></li>
                            <li className="user profile nav-link" style={{display: !this.state.logined ? 'none' : 'block'}} onClick={this.profileToggle}>
                                <a className="nav-link-content"><p>{this.state.user.name + ' (' + this.state.user.username + ')'}</p></a>
                                <i className="fas fa-angle-right"></i>
                                <ul>
                                    <li className="one text-center"><a href={'user/' + this.state.user.username}>Trang cá nhân</a></li>
                                    <li className="two text-center"><a href="settings/account">Thay đổi thông tin</a></li>
                                    <li className="three text-center"><a href="javascript:void(0)" onClick={this.changeTheme} style={{color: 'black', borderRadius: '10px', backgroundColor: 'white'}}>Chuyển sang light theme</a></li>
                                    <li className="four text-center"><a href="javascript:void(0)" onClick={() => this.logOut()}>Đăng xuất</a></li>
                                </ul>
                            </li>
                        </ul>
                        
                    </nav>
                </div>
            </div>
        )
    }
}

module.exports = App