import React, { Component } from 'react'

const test = (username, password) => {
        console.log(username, password)
}

class App extends Component{

    constructor(props){
        super(props);
        this.state={
            alert : ""
        }
        this.handleLogin = this.handleLogin.bind(this)
    }

    componentDidMount(){
        document.title = "Đăng nhập"
        if(screen.width < 700){
            //Collapsed
            document.getElementsByClassName('login')[0].classList.add('collapsed')
        }
    }

    change(e){
        let par_e = e.target.parentNode
        if(e.target.value != ''){
            par_e.childNodes.forEach(e => e.classList.add('field-not-empty'))
        }
        else{
            par_e.childNodes.forEach(e => e.classList.remove('field-not-empty'))
        }
    }

    async handleLogin(e){
        e.preventDefault()
        const target = e.target
        //0 is username, 1 is password
        const Data = {
            [target[0].name] : target[0].value,
            [target[1].name] : target[1].value
        }
        if(Data.username == "" || Data.password == ""){
            if(Data.username == ""){
                target[0].focus();
            }
            if(Data.password == ""){
                target[1].focus();
            }
            this.setState({ alert: "missing" })
        }
        else{
            let res = await fetch('/api/auth', {
                method: 'POST',
                headers:{
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(Data)
            }).then(res => res.json())
            if(res.logined){
                this.setState({ alert: "logined" })
                document.getElementsByClassName('logo')[0].style.fill = "green";
                document.getElementsByClassName('logo')[1].style.fill = "green";
            }
            if(res.err != null){
                this.setState({ alert: "error"})
                document.getElementsByClassName('logo')[0].style.fill = "red";
                document.getElementsByClassName('logo')[1].style.fill = "red";
            }
        }
    }

    render(){
        return (
            <div className="login row">
                <div className="login-form col-md-4 offset-md-2">
                    <h2>Đăng nhập</h2>
                    <hr></hr>
                    <form className="form-group col-md-12" onSubmit={this.handleLogin}>
                        <div className="col-md-12">
                            <div className="col-md-12">
                                <label className="field-name"> Tên tài khoản </label>
                                <input className="field-input" name="username" type="text" placeholder="VD: pythagore1123" onChange={(e) => this.change(e)}></input>
                            </div>
                            <div className="col-md-12">
                                <label className="field-name"> Mật khẩu </label>
                                <input className="field-input" name="password" type="password" placeholder="●●●●●●●●●●●" onChange={(e) => this.change(e)}></input>
                            </div>
                                <button className="btn btn-primary col-md-12" type="submit">Đăng nhập</button>
                        </div>
                    </form>
                    {this.state.alert != "" ? (
                        <Alert stat={this.state.alert}/>
                    ) : null}
                </div>
                <svg className="col-md-4 offset-md-1" viewBox="0 0 600 600">
                    <path className="logo" d="M302.59,40.38,76.17,172.58,77,435.32l225.6,131.37L529,435.32V171.75Zm0,498.87L100.39,422.13l.13-237.08L302.59,67l202.2,118.06.68,237Z"/>
                    <path className="logo" d="M408.89,347.31a28.36,28.36,0,0,0-22.61-5.74l-39-80.81a55.58,55.58,0,0,0,24.33-46.19c0-30.54-24.17-55.29-54-55.29s-54,24.75-54,55.29c0,18.83,8.69,35.33,22.73,45.31L235.43,365.07a39.76,39.76,0,0,0-9.94-1.26c-22.41,0-40.57,18.62-40.57,41.57S203.08,447,225.49,447s40.58-18.62,40.58-41.58a41.84,41.84,0,0,0-15.36-32.55l50.37-105.62a52.89,52.89,0,0,0,32.38.22l38.71,81a28.84,28.84,0,0,0-4,4.33,30.2,30.2,0,0,0,5.3,41.69A28.54,28.54,0,0,0,414.19,389,30.2,30.2,0,0,0,408.89,347.31Z"/>
                </svg>
            </div>
        )
    }
}

class Alert extends Component{

    messages(stat){
        switch(stat){
            case 'logined': return 'Đăng nhập thành công!'
            break
            case 'error': return 'Tên tài khoản hoặc mật khẩu không chính xác'
            break
            case 'missing' : return 'Bạn phải nhập đầy đủ thông tin!'
            break;
        }
    }

    class(stat){
        if(stat == "logined"){ return 'ok' }
        else { return 'error' }
    }

    render(){
        return(
            <div className={'info ' + this.class(this.props.stat)}>
                {this.messages(this.props.stat)}
            </div>
        )
    }
}

module.exports = App