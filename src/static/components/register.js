import React, { Component } from 'react'
import Navbar from '../components/navbar'
import Load from '../components/loader'
import { FreeContest as FC, history, toast } from '../js/action'

const wait = async (ms) => new Promise(res => setTimeout(res, ms))

class App extends Component{

    constructor(props){
        super(props);
        this.state = {
            alert: '',
            registing: false
        }
        this.check_password = this.check_password.bind(this)
        this.handleRegister = this.handleRegister.bind(this)
    }

    check_password(event){
        let check = this.password.value == this.password_retype.value
        console.log(this.password.value, this.password_retype.value)
        if(check){
            document.getElementsByClassName('submit')[0].disabled = false
        }
        else{
            document.getElementsByClassName('submit')[0].disabled = true
        }
    }

    next(){
        document.getElementsByClassName('anchor')[0].animate(
            [
                {marginLeft: '0%'},
                {marginLeft: '-50%'}
            ],{
                duration: 250,
                fill: "forwards"
            }
        )
    }

    back(){
        document.getElementsByClassName('anchor')[0].animate(
            [
                {marginLeft: '-50%'},
                {marginLeft: '0%'}
            ],{
                duration: 250,
                fill: 'forwards'
            }
        )
    }

    focus(e){
        let par_e = e.target.parentNode
        par_e.childNodes.forEach(e => e.classList.add('field-focus'))
    }

    blur(e){
        let par_e = e.target.parentNode
        par_e.childNodes.forEach(e => e.classList.remove('field-focus'))
    }

    async handleRegister(e){
        e.preventDefault()
        const target = e.target
        let name = target[0].value,
            school = target[1].value,
            yob = parseInt(target[2].value),
            username = target[4].value,
            password = target[5].value,
            Data = {name: name, school: school, yob: yob, username: username, password: password}

        let validate = true;
        Object.keys(Data).forEach((key) => {
            if(Data[key] == '' && key != '') { validate = false; }
        })
        if(isNaN(yob)) { validate = false; }

        if(validate){
            this.setState({ registing: true })
            await FC.register(name, school, yob, username, password, async (res) => {
                if(res.err !== undefined){
                    toast.error('Thông báo', 'Đã có lỗi xảy ra')
                    this.setState({ registing: false })
                    document.getElementsByClassName('logo')[0].style.fill = "rgb(9, 15, 29)"
                    document.getElementsByClassName('logo')[1].style.fill = "red"
                }
                else{
                    toast.success('Thông báo', 'Đăng kí thành công, bạn có thể đăng nhập')
                    this.setState({ registing: false })
                    document.getElementsByClassName('logo')[0].style.fill = "rgb(9, 15, 29)"
                    document.getElementsByClassName('logo')[1].style.fill = "green"
                    await wait(2000)
                    history.push('/login')
                }
            })
            
        }
        else{
            toast.error('Thông báo', 'Bạn phải nhập thông tin đầy đủ theo đúng yêu cầu')
        }
    }

    componentDidMount(){
        document.title = 'Đăng kí'

        FC.getSession((res) => {
            if(res.logined){
                toast.error('Thông báo', 'Bạn hãy đăng xuất trước')
                history.push('/')
            }
        })
    }

    render(){
        return(
            <div>
                <Navbar/>
                <div className="register">
                    {this.state.registing ? (
                        <div className="loading">
                            <Load/>
                        </div>
                    ):(
                        <svg viewBox="0 0 600 600">
                            <path className="logo" d="M302.59,40.38,76.17,172.58,77,435.32l225.6,131.37L529,435.32V171.75Zm0,498.87L100.39,422.13l.13-237.08L302.59,67l202.2,118.06.68,237Z"/>
                            <path className="logo" d="M408.89,347.31a28.36,28.36,0,0,0-22.61-5.74l-39-80.81a55.58,55.58,0,0,0,24.33-46.19c0-30.54-24.17-55.29-54-55.29s-54,24.75-54,55.29c0,18.83,8.69,35.33,22.73,45.31L235.43,365.07a39.76,39.76,0,0,0-9.94-1.26c-22.41,0-40.57,18.62-40.57,41.57S203.08,447,225.49,447s40.58-18.62,40.58-41.58a41.84,41.84,0,0,0-15.36-32.55l50.37-105.62a52.89,52.89,0,0,0,32.38.22l38.71,81a28.84,28.84,0,0,0-4,4.33,30.2,30.2,0,0,0,5.3,41.69A28.54,28.54,0,0,0,414.19,389,30.2,30.2,0,0,0,408.89,347.31Z"/>
                        </svg>
                    )}
                    
                    <div className="register-form middle">
                        <h2>Đăng kí tài khoản</h2>
                        <hr></hr>
                        <form className="form-group row" onSubmit={this.handleRegister}>
                            <div className="anchor col-6">
                                <div className="col-sm-12">
                                    <label className="field-name"> Họ và tên </label>
                                    <input className="field-input" name="name" type="text" placeholder="VD: Lê Đình Hải" onFocus={(e) => this.focus(e)} onBlur={(e) => this.blur(e)}></input>
                                </div>
                                <div className="col-sm-12">
                                    <label className="field-name"> Trường </label>
                                    <input className="field-input" name="schoolName" type="text" placeholder="VD: Trường THPT chuyên Lê Quý Đôn" onFocus={(e) => this.focus(e)} onBlur={(e) => this.blur(e)}></input>
                                </div>
                                <div className="col-sm-12">
                                    <label className="field-name"> Năm sinh </label>
                                    <input className="field-input" name="yob" type="text" placeholder="VD: 2001" onFocus={(e) => this.focus(e)} onBlur={(e) => this.blur(e)}></input>
                                </div>
                                <button type="button" className="btn btn-secondary col-sm-6 next" onClick={() => this.next()}>Tiếp tục</button>
                            </div>
                            <div className="col-6">
                                <div className="col-sm-12">
                                    <label className="field-name"> Tên tài khoản </label>
                                    <input className="field-input" name="username" type="text" placeholder="VD: pythagore1123" onFocus={(e) => this.focus(e)} onBlur={(e) => this.blur(e)}></input>
                                </div>
                                <div className="col-sm-12">
                                    <label className="field-name"> Mật khẩu</label>
                                    <input className="field-input" ref={password => this.password = password} name="password" onInput={() => this.check_password()} type="password" placeholder="●●●●●●●●●●●" onFocus={(e) => this.focus(e)} onBlur={(e) => this.blur(e)}></input>
                                </div>
                                <div className="col-sm-12">
                                    <label className="field-name"> Nhập lại mật khẩu </label>
                                    <input className="field-input" ref={password_retype => this.password_retype = password_retype} name="password_retype" onInput={() => this.check_password()} type="password" placeholder="●●●●●●●●●●●" onFocus={(e) => this.focus(e)} onBlur={(e) => this.blur(e)}></input>
                                </div>
                                <button className="btn btn-secondary col-sm-5 back" type="button" onClick={() => this.back()}>Quay lại</button>
                                <button id="submit" disabled="disabled" className="btn btn-primary col-sm-5 submit" type="submit">Đăng kí</button>
                            </div>                
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

class Alert extends Component{

    messages(stat){
        switch(stat){
            case 'ok': return 'Đăng kí thành công, bạn có thể đăng nhập'
            break
            case 'error': return 'Đã có lỗi khi đăng kí'
            break
            case 'missing' : return 'Bạn phải nhập đầy đủ thông tin đúng yêu cầu!'
            break;
        }
    }

    class(stat){
        if(stat == "ok"){ return 'ok' }
        else { return 'error' }
    }

    render(){
        return(
            <div className={'info ' + this.class(this.props.stat)}>
                {this.messages(this.props.stat)}
            </div>
        )
    }
}

module.exports = App