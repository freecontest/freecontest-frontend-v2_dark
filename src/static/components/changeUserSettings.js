import React, { Component } from 'react'
import Init from '../js/init'
import { FreeContest as FC, history, toast } from '../js/action'
import Navbar from '../components/navbar'
import Loader from '../components/loader'
import { Router, Link, Route, Switch, Redirect } from 'react-router-dom'



class App extends Component{

    componentDidMount(){
        document.title = "Thay đổi thông tin"
    }

    render(){
        // console.log('ahihi')
        return(
            <Router history={history}>
                <div>
                    <Route path={this.props.match.url + '/account'} component={Info}/>
                    <Route path={this.props.match.url + '/avatar'} component={Image}/>
                </div>
            </Router>
        )
    }
}

class Info extends Component{

    constructor(props){
        super(props)
        this.state = {
            user: null,
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.check_password = this.check_password.bind(this)
    }

    componentWillMount(){

        FC.getSession((res) => {
            if(!res.logined){
                history.push('/')
                toast.error('Thông báo', 'Bạn cần đăng nhập trước')
            }
            
            if(res.color) res.color = 12
            
            this.setState({ user: res })
        })
    }

    focus(e){
        let par_e = e.target.parentNode
        par_e.childNodes.forEach(e => e.classList.add('field-focus'))
    }

    blur(e){
        let par_e = e.target.parentNode
        par_e.childNodes.forEach(e => e.classList.remove('field-focus'))
    }

    handleSubmit(e){
        e.preventDefault()
        let target = e.target,
            showOnRanking = e.target[0].value == 'on' ? true : false,
            name = e.target[1].value,
            schoolName = e.target[2].value,
            yob = e.target[3].value,
            password = e.target[4].value,
            password_retype = e.target[5].value
        // console.log(showOnRanking, name, schoolName, yob, password, password_retype)

        if(password != ''){
            FC.updateWithP(name, schoolName, yob, password, showOnRanking, (res) => {
                if(res.err != undefined){
                    toast.error('Thông báo', 'Đã có lỗi xảy ra')
                }
                else{
                    toast.success('Thông báo', 'Chỉnh sửa thông tin thành công')
                    history.push({ pathname: '/' })
                    history.push({ pathname: '/settings/account' })
                }
            })
        }
        else{
            FC.updateWithOutP(name, schoolName, yob, showOnRanking, (res) => {
                if(res.err != undefined){
                    toast.error('Thông báo', 'Đã có lỗi xảy ra')
                }
                else{
                    toast.success('Thông báo', 'Chỉnh sửa thông tin thành công')
                    history.push({ pathname: '/' })
                    history.push({ pathname: '/settings/account' })
                }
            })
        }
    }

    check_password(){

        let password = document.getElementById('password').value,
            password_retype = document.getElementById('password_retype').value
        if(password == password_retype) { 
            document.getElementsByClassName('submit')[0].disabled = false
        }
        else{ 
            document.getElementsByClassName('submit')[0].disabled = true
        }
    }

    render(){
        let user = this.state.user
        // console.log(user)

        return(
            <div>
                {user == null ? (
                    <div className="center" style={{width: '400px', height: '400px'}}>
                        <Loader/>
                    </div>
                ): (
                    <div>
                        <Navbar/>
                        <div className="user-info col-8">
                            <center className="header">
                                <center className={'shade ' + Init.colors[user.color]}>
                                    <div className="avatar">
                                        <img src={'https://www.gravatar.com/avatar/' + user.avatar + '?d=mp&s=200'}></img>
                                    </div>
                                </center>
                                <center className="nav col-8">
                                    <strong><p style={{marginTop: '55px'}} className={Init.colors[user.color]}>{user.name}</p></strong>
                                    <p style={{marginLeft: '10px', marginTop: '55px'}}>{'@' + user.username}</p>
                                    <hr></hr>
                                    <a href="settings/account" className="active">Cập nhật thông tin</a>
                                    <a href="settings/avatar" style={{marginLeft: '10px'}}>Cập nhật ảnh đại diện</a>
                                </center>
                            </center>
                            <div className="form col-12">
                                <form onSubmit={this.handleSubmit}>
                                    <h3>Cập nhật thông tin</h3>
                                    <hr></hr>
                                    <input type="checkbox" defaultChecked={user.showOnRanking}></input><b>Hiện trên bảng xếp hạng</b>
                                    <div className="col-12">
                                        <label className="field-name"> Họ và tên </label>
                                        <input className="field-input" name="name" type="text" placeholder="VD: Lê Đình Hải" onFocus={(e) => this.focus(e)} onBlur={(e) => this.blur(e)} defaultValue={user.name}></input>
                                    </div>
                                    <div className="col-12">
                                        <label className="field-name"> Trường </label>
                                        <input className="field-input" name="school" type="text" placeholder="VD: THPT chuyên Lê Quý Đôn" onFocus={(e) => this.focus(e)} onBlur={(e) => this.blur(e)} defaultValue={user.schoolName}></input>
                                    </div>
                                    <div className="col-12">
                                        <label className="field-name"> Năm sinh </label>
                                        <input className="field-input" name="yob" type="text" placeholder="VD: 2001" onFocus={(e) => this.focus(e)} onBlur={(e) => this.blur(e)} defaultValue={user.yob}></input>
                                    </div>
                                    <i>Điền mật khẩu mới nếu bạn muốn thay đổi mật khẩu</i>
                                    <div className="col-12">
                                        <label className="field-name"> Mật khẩu mới </label>
                                        <input id="password" className="field-input" name="password" type="password" placeholder="●●●●●●●●●●●" onFocus={(e) => this.focus(e)} onBlur={(e) => this.blur(e)} onInput={() => this.check_password()}></input>
                                    </div>
                                    <div className="col-12">
                                        <label className="field-name"> Nhập lại mật khẩu </label>
                                        <input ref="password_retype" id="password_retype" className="field-input" name="password_retype" type="password" placeholder="●●●●●●●●●●●" onFocus={(e) => this.focus(e)} onBlur={(e) => this.blur(e)} onInput={() => this.check_password()}></input>
                                    </div>
                                    <center>
                                        <button className="btn btn-primary col-5 submit" type="submit">Cập nhật thông tin</button>
                                    </center>
                                </form>
                            </div>
                        </div>
                        {/* <ReactTooltip place="right"/> */}
                    </div>
                )}
            </div>
        )
    }
}

class Image extends Component{

    constructor(props){
        super(props)
        this.state = {
            user: null
        }

        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentWillMount(){

        FC.getSession((res) => {
            if(!res.logined){
                history.push('/')
                toast.error('Thông báo', 'Bạn cần đăng nhập trước')
            }

            if(res.color) res.color = 12

            this.setState({ user: res })
        })
    }

    focus(e){
        let par_e = e.target.parentNode
        par_e.childNodes.forEach(e => e.classList.add('field-focus'))
    }

    blur(e){
        let par_e = e.target.parentNode
        par_e.childNodes.forEach(e => e.classList.remove('field-focus'))
    }
    
    handleSubmit(e){
        e.preventDefault()
        let target = e.target,
            email = e.target[0].value.toLowerCase()
        // console.log(email)

        FC.updateAvt(email, (res) => {
            if(res.err != undefined){
                toast.error('Thông báo', 'Đã có lỗi xảy ra')
            }
            else{
                toast.error('Thông báo', 'Chỉnh sửa thông tin thành công')
                history.push({ pathname: '/' })
                history.push({ pathname: '/settings/avatar' })
            }
        })
    }

    render(){
        const { user } = this.state

        return(
            <div>
                {user == null ? (
                    <div className="center" style={{width: '400px', height: '400px'}}>
                        <Loader/>
                    </div>
                ): (
                    <div>
                        <Navbar/>
                        <div className="user-info col-8">
                            <center className="header">
                                <div className={'shade ' + Init.colors[user.color]}>
                                    <div className="avatar">
                                        <img src={'https://www.gravatar.com/avatar/' + user.avatar + '?d=mp&s=200'}></img>
                                    </div>
                                </div>
                                <center className="nav col-8">
                                    <strong><p style={{marginTop: '55px'}} className={Init.colors[user.color]}>{user.name}</p></strong>
                                    <p style={{marginLeft: '10px', marginTop: '55px'}}>{'@' + user.username}</p>
                                    <hr></hr>
                                    <a href="settings/account">Cập nhật thông tin</a>
                                    <a href="settings/avatar" className="active" style={{marginLeft: '10px'}}>Cập nhật ảnh đại diện</a>
                                </center>
                            </center>
                            <div className="form col-12">
                                <form onSubmit={this.handleSubmit}>
                                    <h3>Cập nhật ảnh đại diện</h3>
                                    <hr></hr>
                                    <p>Để thêm ảnh đại diện, bạn hãy cung cấp email ở bên dưới.</p>
                                    <p>Để cập nhật ảnh đại diện, hãy truy cập <a href="https://www.gravatar.com/">Gravatar</a>.</p>
                                    <div className="col-12">
                                        <label className="field-name"> Email </label>
                                        <input className="field-input" name="password_retype" type="text" placeholder="VD: ledinhhai1312@gmail.com" onFocus={(e) => this.focus(e)} onBlur={(e) => this.blur(e)}></input>
                                    </div>
                                    <center>
                                        <button className="btn btn-primary col-5 submit" type="submit">Cập nhật thông tin</button>
                                    </center>
                                </form>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        )
    }
}

module.exports = App