import React, { Component } from 'react'
import moment from 'moment'
import ReactTooltip from 'react-tooltip'
import Navbar from '../components/navbar'
import Loader from '../components/loader'
import {FreeContest as FC, history} from '../js/action'
import Init from '../js/init'

const wait = async (ms) => new Promise(res => setTimeout(res, ms))

class App extends Component{
    constructor(props){
        super(props)
        this.state = {
            logined: null,
            user: null,
            posts: null,
            contests: null,
            topten: null,
            pages: null,
            curPage: null,
            new_posts: null,
        }

        this.back = this.back.bind(this)
        this.next = this.next.bind(this)
        this.toPage = this.toPage.bind(this)
        this.update = this.update.bind(this)
    }

    async componentDidMount(){
        document.title = 'Trang chủ'

        moment.locale('vi')

        FC.getSession((res) => {
            this.setState({ logined: res.logined, user: res })
        })
        
        FC.getPosts((res) => {
            let temp1 = []
            res.map(p => {
                let doc = new DOMParser().parseFromString('<div>' + p.HTMLContent + '</div>', 'text/xml'),
                    img = doc.getElementsByTagName('img')[0],
                    url = img == undefined ? null : img.getAttribute('src'),
                    title = p.title,
                    description = this.toPlain(p.HTMLContent),
                    author = p.author,
                    show = p.show,
                    id = p.id,
                    updated = p.updatedAt
                // console.log(doc)
                temp1.push({ url: url, title: title, description: description, author: author, show: show, id: id, updated: updated })
            })

            let temp2 = []
            for(var i = 0; i < temp1.length; i += 7){
                temp2.push(temp1.slice(i, i + 7))
            }

            this.setState({ new_posts: temp2, pages: temp2.length, curPage: 0 })
            // console.log(this.state.new_posts)
        })

        FC.getContest((contests) => {
            // console.log(contests)
            let res, recent = [], upcoming = [], present = []
            contests.map((c) => {
                let startedTime = (new Date(c.startTime)).getTime();
                let endTime = (new Date(c.endTime)).getTime();
                let currentTime = new Date().getTime();

                if (endTime < currentTime) {
                    recent.push(c);
                } else
                if (startedTime > currentTime) {
                    upcoming.push(c);
                } else
                if (startedTime <= currentTime && currentTime <= endTime) {
                    present.push(c);
                };
            })

            res = {recent: recent, upcoming: upcoming, present: present}
            res.recent.reverse()
            // console.log(res)
            this.setState({ contests: res })
        }, 10)

        FC.getUsers((res) => {
            for(var i = 0; i < 10; i++){
                res[i].rank = i + 1;
            }
            this.setState({ topten: res })
        }, 10)

        // console.log(Init)
    }

    toPlain(html){
        html = html.replace(/<style([\s\S]*?)<\/style>/gi, '.');
        html = html.replace(/<script([\s\S]*?)<\/script>/gi, '.');
        html = html.replace(/<\/div>/ig, '.\n');
        html = html.replace(/<\/li>/ig, '.\n');
        html = html.replace(/<li>/ig, '  *  ');
        html = html.replace(/<\/ul>/ig, '.\n');
        html = html.replace(/<\/p>/ig, '.\n');
        html = html.replace(/<br\s*[\/]?>/gi, ".\n");
        html = html.replace(/<[^>]+>/ig, ' ');
        return html
    }

    next(){
        if(this.state.curPage == this.state.pages - 1) return;
        this.setState({ curPage: this.state.curPage + 1 })
    }

    back(){
        if(this.state.curPage == 0) return;
        this.setState({ curPage: this.state.curPage - 1 })
    }

    toPage(e){
        let pid = parseInt(e.target.value) - 1
        if(pid < 0 || pid >= this.state.pages) {
            document.getElementById('pageId').value = this.state.curPage + 1
            return;
        }
        document.getElementById('pageId').value = pid + 1
        this.setState({ curPage: pid })
    }

    update(){
        document.getElementById('pageId').value = this.state.curPage + 1
    }

    render(){
        return(
            <div>
                <Navbar/>
                <div className="row">
                    <div className="col-md-4 order-md-2">
                        {this.state.contests == null ? (
                            <div style={{height: '100vh'}}>
                                <div className="center" style={{width: '200px', height: '200px'}}>
                                    <Loader/>
                                </div>
                            </div>
                        ):(
                            <div className="contests">
                                <div className="contest-title">
                                    <h3>Contests</h3>
                                </div>
                                <Contest contests={this.state.contests}/>
                            </div>
                        )}
                    </div>
                    <div className="col-md-8 order-md-1"> 
                        {this.state.new_posts == null ? (
                            <div style={{height: '100vh'}}>
                                <div className="center" style={{width: '300px', height: '300px'}}>
                                    <Loader/>
                                </div>
                            </div>
                        ):(
                            <div className="new_posts">
                                {Object.keys(this.state.new_posts).map(pid => (
                                    <div className="page" key={pid} style={{ display: this.state.curPage == pid ? '' : 'none' }}>
                                        {this.state.new_posts[pid].map(p => (
                                            <New_post style={{display: p.show == false ? 'none': ''}} key={p.id} post={p}/>
                                        ))}
                                    </div>
                                ))}
                                <center className="pages">
                                    <div className="navigate" onClick={() => this.back()}><i className="fas fa-angle-left"></i></div>
                                    <label>{this.state.curPage + 1}</label>
                                    <div className="navigate" onClick={() => this.next()}><i className="fas fa-angle-right"></i></div>
                                </center>
                                <ReactTooltip id="right" place="right"/>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        )
    }
}

class New_post extends Component{

    constructor(props){
        super(props)
        
        this.onClick = this.onClick.bind(this)
    }

    onClick(){
        history.push('post/' + this.props.post.id)
    }

    render(){
        const { post } = this.props

        return(
            <div data-for="right" data-tip="Nhấp chuột để xem thêm" className="post row" onClick={() => this.onClick()}>
                <div className="img col-3">
                    {post.url == null ? (
                        <svg viewBox="0 0 600 600">
                            <path d="M302.59,40.38,76.17,172.58,77,435.32l225.6,131.37L529,435.32V171.75Zm0,498.87L100.39,422.13l.13-237.08L302.59,67l202.2,118.06.68,237Z"/>
                            <path d="M408.89,347.31a28.36,28.36,0,0,0-22.61-5.74l-39-80.81a55.58,55.58,0,0,0,24.33-46.19c0-30.54-24.17-55.29-54-55.29s-54,24.75-54,55.29c0,18.83,8.69,35.33,22.73,45.31L235.43,365.07a39.76,39.76,0,0,0-9.94-1.26c-22.41,0-40.57,18.62-40.57,41.57S203.08,447,225.49,447s40.58-18.62,40.58-41.58a41.84,41.84,0,0,0-15.36-32.55l50.37-105.62a52.89,52.89,0,0,0,32.38.22l38.71,81a28.84,28.84,0,0,0-4,4.33,30.2,30.2,0,0,0,5.3,41.69A28.54,28.54,0,0,0,414.19,389,30.2,30.2,0,0,0,408.89,347.31Z"/>
                        </svg>
                    ):(
                        <img src={post.url}></img>
                    )}
                </div>
                <div className="content col-9">
                    <h3 className="title">{post.title}</h3>
                    <p className="description">{post.description}</p>
                    <pre className="author">Được đăng bởi <p className="dark-red">{post.author}</p> / Cập nhật lần cuối: <p>{' ' + moment(post.updated).from(moment())}</p></pre>
                </div>
            </div>
        )
    }
}

class Contest extends Component{

    render(){
        const { contests } = this.props
        // console.log("render", contests)
        return(
            <div>
                {contests.present.length > 0 ? (
                    <div className="contest">
                        {/* {contests.present.length > 0 ? (
                            <div className="present">
                                <div className="contest-header">
                                    <h3>Contest đang diễn ra</h3>
                                </div>
                                <div className="contest-content">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Bắt đầu</th>
                                                <th>Tên contest</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {contests.present.map(c => (
                                                <tr key={c.id}>
                                                    <td><a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).format('llll')}</a></td>
                                                    <td><a href={c.facebool_url}>{c.name}</a></td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="contest-footer">
                                    <a href="contest">[Xem tất cả]</a>
                                </div>
                            </div>
                        ):(<div></div>)} */}
                        <div className="present">
                            <div className="contest-header">
                                <h4>Contest đang diễn ra</h4>
                                <hr></hr>
                            </div>
                            <center className="contest-content">
                                <h3>{contests.present[0].name}</h3>
                                <p>{'Bắt đầu ' + moment(contests.present[0].startTime).from(moment())}</p>
                                {contests.present[0].isRegistered ? (
                                    <a href={'autologin/' + contests.present[0].id}><button className="btn btn-primary">Tham gia</button></a>
                                ):(
                                    <a href={'register/new/' + contests.present[0].id}><button className="btn btn-primary">Đăng kí</button></a>
                                )}
                            </center>
                        </div>
                        <ReactTooltip place="left"/>
                    </div>
                ):(
                    <div></div>
                )}
                
                <div className="contest" style={{display: contests.upcoming.length > 0 ? 'block' : 'none'}}>
                    <div className="upcoming">
                        <div className="contest-header">
                            <h4>Contest sắp tới</h4>
                            <hr></hr>
                        </div>
                        <div className="contest-content">
                            <table className="intergrated-table">
                                <thead>
                                    <tr>
                                        <th>Bắt đầu</th>
                                        <th>Tên contest</th>
                                    </tr>
                                </thead>
                                {contests.upcoming.length > 0 ? (
                                        <tbody>
                                        {contests.upcoming.map(c => (
                                            <tr key={c.id}>
                                                <td><a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).format('llll')}</a></td>
                                                <td><a href={c.facebool_url}>{c.name}</a></td>
                                            </tr>
                                        ))}
                                    </tbody>
                                ):(
                                    <tbody>
                                        <tr>
                                            <td colSpan="2">Không có contest sắp tới</td>
                                        </tr>
                                    </tbody>
                                )}
                            </table>
                        </div>
                        {/* <div className="contest-footer">
                            <a href="contest">[Xem tất cả]</a>
                        </div> */}
                    </div>
                    <ReactTooltip place="left"/>
                </div>
                <div className="contest" style={{display: contests.recent.length > 0 ? 'block' : 'none'}}>
                    <div className="recent">
                        <div className="contest-header">
                            <h4>Contest đã qua</h4>
                            <hr></hr>
                        </div>
                        <div className="contest-content">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Bắt đầu</th>
                                        <th>Tên contest</th>
                                    </tr>
                                </thead>
                                {contests.recent.length > 0 ? (
                                    <tbody>
                                        {contests.recent.map(c => (
                                            <tr key={c.id}>
                                                <td><a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).format('llll')}</a></td>
                                                <td><a href={c.facebool_url}>{c.name}</a></td>
                                            </tr>
                                        ))}
                                    </tbody>
                                ):(
                                    <tbody>
                                        <tr>
                                            <td colSpan="2">Không có contest đã qua</td>
                                        </tr>
                                    </tbody>
                                )}
                            </table>
                        </div>
                        {/* <div className="contest-footer">
                            <a href="contest">[Xem tất cả]</a>
                        </div> */}
                    </div>
                    <ReactTooltip place="left"/>
                </div>
            </div>
        )
    }
}

class Ranking extends Component{

    //Too lazy to redo :v
    render(){
        const { topten } = this.props
        return(
            <div className="ranking">
                    {topten.length > 0 ? (
                        <div className="present">
                            <div className="ranking-content">
                                <table>
                                    <thead>
                                        <tr>
                                            <th className="text-center">#</th>
                                            <th className="text-center">User</th>
                                            <th className="text-center">Rating</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {topten.map(u => (
                                            <tr key={u.id}>
                                                <td className="text-center">{u.rank}</td>
                                                <td className="text-center"><a href={'user/' + u.username}><p className={Init.colors[u.color]}>{u.username}</p></a></td>
                                                <td className="text-center">{u.rating}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    ):(<div></div>)}
                </div>
        )
    }
}

module.exports = { App, Contest }