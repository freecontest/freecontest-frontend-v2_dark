import React, { Component } from 'react'
import { FreeContest as FC, history, toast } from '../js/action'
import Init from '../js/init'
import Navbar from '../components/navbar'
import Loader from '../components/loader'
import ReactTooltip from 'react-tooltip' 
import moment from 'moment'

class App extends Component{

    constructor(props){
        super(props)

        this.state = {
            user: null,
            regedContests: null,
        }

    }

    componentDidMount(){
        moment.locale('vi')

        // console.log(this.props.match.params.uid)
        document.title = 'FreeContest'

        FC.getUser(this.props.match.params.uid, (res) => {
            if(res.err != undefined) {
                toast.error('Thông báo', 'Người dùng không tồn tại')
                history.push('/')
            }
            else{
                /* Updated in another version */
                // let mxRating = -1
                // res.logsUpdateRating.map(log => mxRating = Math.max(mxRating, log.rating))
                // res.mxRating = mxRating
                // if(mxRating != -1) { res.curRating = res.logsUpdateRating[res.logsUpdateRating.length - 1].rating }
                // else { res.curRating = -1 }

                res.curRating = -1
                if(res.color) res.color = 12
                this.setState({ user: res })
                document.title = this.state.user.name + ' | FreeContest'
            }    
            
            FC.getContestX(res.id, (res) => {
                // console.log(res)
                if(res.err != undefined) return;            

                let contests = []

                res.map(c => {
                    if(c.isRegistered) { contests.unshift(c) }
                })

                this.setState({ regedContests: contests })
                
            })
        })

        
    }

    render(){
        return(
            <div>
                <Navbar/>
                <div className="profile-page col-sm-8" style={{marginTop: '100px'}}>
                    <div className="info">
                        {this.state.user == null ? (
                            <div style={{ height: '300px' }}>
                                <div className="center" style={{width: '200px', height: '200px'}}>
                                    <Loader/>
                                </div>
                            </div>
                        ):(
                            <div className="row">
                                <center className={Init.colors[this.state.user.color]}>
                                    <div>
                                        <div className="glow" style={{display: this.state.user.role == 'admin' ? '' : 'none'}}/>
                                        <img src={'https://www.gravatar.com/avatar/' + this.state.user.avatar + '?d=mp&s=200'}></img>
                                    </div>
                                </center>
                                <div className="details">
                                    <strong className={Init.colors[this.state.user.color == 11 ? 10 : this.state.user.color]}>{Init.titles[this.state.user.color]}</strong>
                                    <h3><p className={Init.colors[this.state.user.color]}>{this.state.user.name}</p></h3>
                                    {/* <p className={Init.colors[this.state.user.color == 11 ? 10 : this.state.user.color]}>{(this.state.user.curRating == -1) ? '' : this.state.user.curRating}</p> */}
                                    <p>{'@' + this.state.user.username}</p>
                                    <hr></hr>
                                    <p>{'Đang học tại ' + this.state.user.schoolName}</p>
                                    <p>{'Đã tham gia ' + moment(this.state.user.createdAt).from(moment())}</p>
                                </div>
                            </div>
                        )}
                    </div>
                    <div className="chart">
                    
                    </div>
                    <div className="contest">
                        {this.state.regedContests == null ? (
                            <div style={{ height : '300px' }}>
                                <div className="center" style={{width: '200px', height: '200px'}}>
                                    <Loader/>
                                </div>
                            </div>
                        ):(
                            <div>
                                <div className="present">
                                    <div className="contest-header">
                                        <h3>Contest đã đăng kí</h3>
                                    </div>
                                    <div className="panel panel-default">
                                        <div className="table-responsive-md">
                                            <table className="table-dark">
                                                <thead>
                                                    <tr>
                                                        <th className="text-center">Bắt đầu</th>
                                                        <th className="text-center">Tên contest</th>
                                                        <th className="text-center">Đăng kí</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.regedContests.map(c => (
                                                        <tr key={c.id}>
                                                            <td className="text-center"><a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).format('llll')}</a></td>
                                                            <td className="text-center"><a href={c.facebool_url}>{c.name}</a></td>
                                                            <td className="text-center"><a data-tip={moment(c.updatedAt).from(moment())}>{moment(c.updatedAt).format('llll')}</a></td>
                                                        </tr>
                                                    ))}
                                                    <tr style={{display: this.state.regedContests.length == 0 ? '' : 'none'}}>
                                                        <td className="text-center" colSpan="3">Bạn chưa đăng kí contest nào</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <ReactTooltip place="left"/>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        )
    }
}

module.exports = App