import React, { Component } from 'react'
import Init from '../js/init'
import { FreeContest as FC, history, toast } from '../js/action'
import moment from 'moment'
import Navbar from '../components/navbar'
import Loader from '../components/loader'

class App extends Component{

    constructor(props){
        super(props)
        this.state = {
            user: null,
            logined: false,
            post: null
        }
    }

    componentDidMount(){
        let { pid } = this.props.match.params
        console.log(pid)
        FC.getPostWId(pid, (res) => {
            this.setState({ post: res[0] })
        })

        FC.getSession((res) => {
            if(res.logined && res.role == 'admin'){
                console.log('admin')
                Array.from(document.getElementsByClassName('admin')).forEach(e => e.style.display = 'block')
            }
            this.setState({ logined: res.logined, user: res })
        })
    }

    deletePost(pid){
        console.log('deleted')
        FC.deletePost(pid, async (res) => {
            if(res.err != undefined){
                toast.error('Thông báo', 'Đã có lỗi xảy ra')
            }
            else{
                toast.success('Thông báo', 'Xóa bài viết thành công')
                history.push({ pathname: '/' })
            }
        })
    }

    render(){
        const { post } = this.state
        return(
            <div>
                <Navbar/>
                {this.state.post == null ? (
                    <div className="center" style={{width: '400px', height: '400px'}}>
                        <Loader/>
                    </div>
                ):(
                    <div className="post" >
                        <div className="post-heading">
                            <h3>{post.title}</h3>
                            <pre className="author">Được đăng bởi <a href={"user/" + post.author} className="dark-red">{post.author}</a> / Cập nhật lần cuối: <p>{' ' + moment(post.updated).from(moment())}</p></pre>
                            <div className="admin">
                                <a href={ "https://freecontest.xyz/admin/edit/post/" + post.id }>Chỉnh sửa bài viết</a>
                                <a href="javascript:void(0)" onClick={ () => this.deletePost(post.id) }>Xóa bài viết</a>
                            </div>
                        </div>
                        <div className="post-content">
                            <div dangerouslySetInnerHTML={{ __html: post.HTMLContent }}/>
                        </div>
                        <div className="post-footer">
                            <a href="#">
                                <i className="fas fa-comments"></i><strong>Bình luận({post.cntComment})</strong>
                            </a>
                        </div>
                    </div>
                )}
            </div>
        )
    }
}


module.exports = App