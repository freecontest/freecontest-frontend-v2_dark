import React, { Component } from 'react'
import moment from 'moment'
import Navbar from '../components/navbar'
import Load from '../components/loader'
import ReactTooltip from 'react-tooltip'
import { FreeContest as FC } from '../js/action'
import Init from '../js/init'
import { timingSafeEqual } from 'crypto';

const toTime = (ms) => {
    var hours = Math.floor((ms % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((ms % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((ms % (1000 * 60)) / 1000);
    if(minutes < 10) minutes = '0' + minutes
    if(seconds < 10) seconds = '0' + seconds
    return hours + ':' + minutes + ':' + seconds
}

class App extends Component{

    constructor(props){
        super(props)
        this.state={
            contests: null,
            logined: false,
            now: new Date().getTime(),
            curPage: 0,
            pages: 0,
            user: null
        }

        this.next = this.next.bind(this)
        this.back = this.back.bind(this)
    }

    componentWillMount(){
        FC.getSession((res) => {
            this.setState({ user: res, logined: res.logined })
        })
    }

    componentDidMount(){
        moment.locale('vi')

        document.title="Contest"

        FC.getContest((contests) => {
            // console.log(contests)
            let res, recent = [], upcoming = [], present = [], registered = []
                contests.forEach((c) => {
                    let startedTime = (new Date(c.startTime)).getTime();
                    let endTime = (new Date(c.endTime)).getTime();
                    let currentTime = new Date().getTime();

                    if (endTime < currentTime) {
                        recent.push(c);
                    } else
                    if (startedTime > currentTime) {
                        upcoming.push(c);
                    } else
                    if (startedTime <= currentTime && currentTime <= endTime) {
                        present.push(c);
                    };
                    
                    if(c.isRegistered) registered.push(c)
                })

            let temp = []
            recent = recent.reverse()

            for(var i = 0; i < recent.length; i += 7){
                temp.push(recent.slice(i, i + 7))
            }

            recent = temp
            this.setState({ pages: recent.length })

            res = {present: present, upcoming: upcoming, recent: recent, registered: registered.reverse()}
            console.log(res)
            this.setState({ contests: res })
        }, 10)
        
        setInterval(() => {
            this.setState({ now: new Date().getTime() });
        }, 1000)
    }

    toggle(e){
        let ele = e.target
        while(!ele.classList.contains('contest')) ele = ele.parentNode

        if(ele.classList.contains('open')){
            ele.classList.remove('open')
        }
        else{
            ele.classList.add('open')
        }
    }

    next(){
        if(this.state.curPage == this.state.pages - 1) return;
        this.setState({ curPage: this.state.curPage + 1 })
    }

    back(){
        if(this.state.curPage == 0) return;
        this.setState({ curPage: this.state.curPage - 1 })
    }

    render(){
        return(
            <div>
                <Navbar/>    
                <div className="contest-page row">
                    {this.state.contests == null ? (
                        <div className="center" style={{width: '200px', height: '200px'}}>
                            <Load/>
                        </div>
                    ):(
                        <div className="col-10 offset-1">
                            {/* <div className="present">
                                <h3>Contest đang diễn ra</h3>
                                <div className="panel panel-default">
                                    <div className="table-responsive-md">
                                        <table className="table-dark">
                                            <thead>
                                                <tr>
                                                    <th width="18%" className="text-center">Bắt đầu</th>
                                                    <th className="text-center">Tên Contest</th>
                                                    <th width="10%" className="text-center">Thời lượng</th>
                                                    <th width="15%" className="text-center">Người ra đề</th>
                                                    <th width="10%"></th>
                                                    <th width="8%"></th>
                                                </tr>
                                            </thead>
                                            {this.state.contests.present.length > 0 ? (
                                                <tbody>
                                                    {this.state.contests.present.map(c => (
                                                        <tr key={c.id}>
                                                            <td className="text-center">
                                                                <a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).from(moment())}</a>
                                                            </td>
                                                            <td className="text-center">
                                                                <a href={c.facebook_url == '' ? 'javascript:void(0)' : c.facebook_url}>{c.name}</a>
                                                            </td>
                                                            <td className="text-center">
                                                                {c.duration} tiếng
                                                            </td>
                                                            <td className="text-center">
                                                                {c.wrrts.map(w => (
                                                                    <div key={w.username}><a className={Init.colors[w.color]} href={'user/' + w.username}>{w.username}</a></div>
                                                                ))}
                                                            </td>
                                                            <td className="text-center">
                                                                <a href={'autologin/' + c.id} style={{display: c.isRegistered ? 'block' : 'none'}}>Tham gia</a>
                                                                <a href={'register/new/' + c.id} style={{display: !c.isRegistered ? 'block' : 'none'}}>Đăng kí</a>
                                                                <a href={'https://freecontest.xyz/admin/edit/contest/' + c.id} style={{display: (this.state.logined && this.state.user.role == 'admin') ? 'block' : 'none'}}>Sửa</a>
                                                            </td>
                                                            <td className="text-center">
                                                                <div><a href="https://code.freecontest.xyz/ranking" target="_blank">Ranking</a></div>
                                                                <div>
                                                                    <a href={'registered/' + c.id}>
                                                                        <strong><i className="fas fa-users"></i> x{c.cntReg}</strong>
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    ))}
                                                </tbody>):(
                                                <tbody>
                                                    <tr><td className="text-center" colSpan="6"><i>Không có contest nào đang diễn ra</i></td></tr>
                                                </tbody>
                                            )}
                                        </table>
                                    </div>
                                </div>
                            </div> */}
                            {this.state.contests.present.length > 0 ? (
                                <div className="present">
                                    <h2>Contest đang diễn ra</h2>
                                    <div className="contest-content">
                                        {this.state.contests.present.map(c => (
                                            <div key={c.id} className="contest open">
                                                <div className="glow"/>
                                                <div className="header" role="button" onClick={(e) => this.toggle(e)}>
                                                    <h3>{c.name}</h3>
                                                    <div className="caret">
                                                        <p>{moment(c.startTime).format('L')}</p>  <i className="fas fa-angle-left"></i>
                                                    </div>
                                                    <div className="pointer" style={{right: 100 * (new Date(c.endTime).getTime() - this.state.now) / (c.duration * 3600000) + '%'}}/>
                                                    <div className="progress" style={{clipPath: 'inset(0 ' + 100 * (new Date(c.endTime).getTime() - this.state.now) / (c.duration * 3600000) + '% 0 0)'}}/>
                                                </div>
                                                <div className="clip">
                                                    <div className="content row">
                                                        <div className="info col-md-8">
                                                            <p><i className="fas fa-clock"/>{' Thời gian còn lại ' + toTime(new Date(c.endTime).getTime() - this.state.now) + ' / Thời lượng: ' + c.duration + ' tiếng'}</p>
                                                            <p><a href={'registered/' + c.id}><i className="fas fa-users"/> {c.cntReg}</a> thí sinh đã đăng kí</p>
                                                            {c.isRegistered ? (
                                                                <a href={'autologin/' + c.id}><button className="btn btn-primary">Tham gia</button></a>
                                                            ):(
                                                                <a href={'register/new/' + c.id}><button className="btn btn-primary">Đăng kí</button></a>
                                                            )}
                                                            <div style={{display: this.state.logined && this.state.user.role == 'admin' ? '' : 'none'}}></div>
                                                        </div>
                                                        <div className="writers col-md-4">
                                                            <b>Người ra đề</b>
                                                            <hr></hr>
                                                            {c.wrrts.map(w => (
                                                                <div key={w.username}><a href={'user/' + w.username} className={Init.colors[w.color]}>{w.username}</a></div>
                                                            ))}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            ):(
                                <div></div>
                            )}
                            {/* <div className="upcoming">
                                <h3>Contest sắp tới</h3>
                                <div className="panel panel-default">
                                    <div className="table-responsive-md">
                                        <table className="table-dark">
                                            <thead>
                                                <tr>
                                                    <th width="18%" className="text-center">Bắt đầu</th>
                                                    <th className="text-center">Tên Contest</th>
                                                    <th width="10%" className="text-center">Thời lượng</th>
                                                    <th width="15%" className="text-center">Người ra đề</th>
                                                    <th width="10%"></th>
                                                    <th width="8%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.contests.upcoming.map(c => (
                                                    <tr key={c.id}>
                                                        <td className="text-center">
                                                            {/* <a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).format('llll')}</a> */}
                                                            {/* <a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).from(moment())}</a>
                                                        </td>
                                                        <td className="text-center">
                                                            <a href={c.facebook_url == '' ? 'javascript:void(0)' : c.facebook_url}>{c.name}</a>
                                                        </td>
                                                        <td className="text-center">
                                                            {c.duration} tiếng
                                                        </td>
                                                        <td className="text-center">
                                                            {c.wrrts.map(w => (
                                                                <div key={w.username}><a className={Init.colors[w.color]} href={'user/' + w.username}>{w.username}</a></div>
                                                            ))}
                                                        </td>
                                                        <td className="text-center">
                                                            <div>
                                                                <a href={'/register/new/' + c.id} style={{display: c.isRegistered ? 'none' : 'block'}}> Đăng kí </a>
                                                                <span style={{color: 'rgb(50, 195, 50)', fontWeight: 'bold', display: !c.isRegistered ? 'none' : 'block'}}> Đã đăng kí </span>
                                                            </div>
                                                            <div style={{display: (this.state.logined && this.state.user.role == 'admin') ? 'block' : 'none'}}>
                                                                <a href={'https://freecontest.xyz/admin/edit/contest/' + c.id}> Sửa </a>
                                                            </div>
                                                        </td>
                                                        <td className="text-center">
                                                            <div>
                                                                <a href={'registered/' + c.id}>
                                                                    <strong><i className="fas fa-users"></i> x{c.cntReg}</strong>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                ))}
                                                <tr style={{display: this.state.contests.upcoming.length > 0 ? 'none' : ''}}><td className="text-center" colSpan="6">Không có contest nào sắp tới</td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> */}
                            {this.state.contests.upcoming.length > 0 ? (
                                <div className="upcoming">
                                    <h2>Contest sắp tới</h2>
                                    <div className="contest-content">
                                        {this.state.contests.upcoming.map(c => (
                                            <div key={c.id} className="contest open">
                                                <div className="glow"/>
                                                <div className="header" role="button" onClick={(e) => this.toggle(e)}>
                                                    <h3>{c.name}</h3>
                                                    <div className="caret">
                                                        <p>{moment(c.startTime).format('L')}</p>  <i className="fas fa-angle-left"></i>
                                                    </div>
                                                </div>
                                                <div className="clip">
                                                    <div className="content row">
                                                        <div className="info col-md-8">
                                                            <p><i className="fas fa-clock"/>{' Bắt đầu vào ' + moment(c.startTime).from(moment()) + ' / Thời lượng: ' + c.duration + ' tiếng'}</p>
                                                            <p><a href={'registered/' + c.id}><i className="fas fa-users"/> {c.cntReg}</a> thí sinh đã đăng kí</p>
                                                            {c.isRegistered ? (
                                                                <a href={'autologin/' + c.id}><button className="btn btn-primary">Tham gia</button></a>
                                                            ):(
                                                                <a href={'register/new/' + c.id}><button className="btn btn-primary">Đăng kí</button></a>
                                                            )}
                                                            <div style={{display: this.state.logined && this.state.user.role == 'admin' ? '' : 'none'}}></div>
                                                        </div>
                                                        <div className="writers col-md-4">
                                                            <b>Người ra đề</b>
                                                            <hr></hr>
                                                            {c.wrrts.map(w => (
                                                                <div key={w.username}><a href={'user/' + w.username} className={Init.colors[w.color]}>{w.username}</a></div>
                                                            ))}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            ):(
                                <div></div>
                            )}
                            {/* <div className="recent">
                                <h3>Contest đã qua</h3>
                                <div className="panel panel-default">
                                    <div className="table-responsive-md">
                                        <table className="table-dark">
                                            <thead>
                                                <tr>
                                                    <th width="18%" className="text-center">Bắt đầu</th>
                                                    <th className="text-center">Tên Contest</th>
                                                    <th width="13%" className="text-center">Thời lượng</th>
                                                    <th width="20%" className="text-center">Người ra đề</th>
                                                    <th width="10%" className="text-center">Luyện tập</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.contests.recent.map(c => (
                                                    <tr key={c.id}>
                                                        <td className="text-center">
                                                            {/* <a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).format('llll')}</a> */}
                                                            {/* <a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).from(moment())}</a>
                                                        </td>
                                                        <td className="text-center">
                                                            <a href={c.facebook_url == '' ? 'javascript:void(0)' : c.facebook_url}>{c.name}</a>
                                                        </td>
                                                        <td className="text-center">
                                                            {c.duration} tiếng
                                                        </td>
                                                        <td className="text-center">
                                                            {/* {console.log(c.id, c.wrrts)} */}
                                                            {/* {c.wrrts.map(w => (
                                                                <div key={w.username}><a className={Init.colors[w.color]} href={'user/' + w.username}>{w.username}</a></div>
                                                            ))}
                                                        </td>
                                                        <td className="text-center">
                                                            <div>
                                                                {c.practice_url == null ? (
                                                                    <strong>
                                                                        <i className="fas fa-times"></i>
                                                                    </strong>
                                                                ):(
                                                                    <a href={c.practice_url}>Luyện tập</a>
                                                                )}
                                                            </div>
                                                        </td>
                                                    </tr>
                                                ))}
                                                <tr style={{display: this.state.contests.recent.length > 0 ? 'none' : ''}}><td className="text-center" colSpan="5">Không có contest nào đã qua</td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> */}
                            {this.state.contests.recent.length > 0 ? (
                                <div className="recent">
                                    <h2>Contest đã qua</h2>
                                    <div className="contest-content">
                                        {Object.keys(this.state.contests.recent).map(pid => (
                                            <div key={pid} style={{display: this.state.curPage == pid ? '' : 'none'}}>
                                                {this.state.contests.recent[pid].map(c => (
                                                    <div key={c.id} className="contest">
                                                        {/* <div className="progress" style={{clipPath: 'inset(0 0 0 0)'}}/> */}
                                                        <div className="header" role="button" onClick={(e) => this.toggle(e)}>
                                                            <h3>{c.name}</h3>
                                                            <div className="caret">
                                                                <p>{moment(c.startTime).format('L')}</p>  <i className="fas fa-angle-left"></i>
                                                            </div>
                                                        </div>
                                                        <div className="clip">
                                                            <div className="content row">
                                                                <div className="info col-md-8">
                                                                    <p><i className="fas fa-clock"/>{' Contest đã kết thúc / Thời lượng: ' + c.duration + ' tiếng'}</p>
                                                                    <p><i className="fas fa-users"/> <a href={'registered/' + c.id}>{c.cntReg}</a> thí sinh đã đăng kí</p> 
                                                                    <strong style={{color: '#44de44'}}>Luyện tập: <a href={c.practice_url}><button disabled={c.practice_url == null ? true : false} className="btn btn-primary btn-hackerrank"><i className="fab fa-hackerrank"/> HackerRank</button></a></strong>
                                                                    <div style={{display: this.state.logined && this.state.user.role == 'admin' ? '' : 'none'}}></div>
                                                                </div>
                                                                <div className="writers col-md-4">
                                                                    <b>Người ra đề</b>
                                                                    <hr></hr>
                                                                    {c.wrrts.map(w => (
                                                                        <div key={w.username}><a href={'user/' + w.username} className={Init.colors[w.color]}>{w.username}</a></div>
                                                                    ))}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))}
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            ):(
                                <div></div>
                            )}
                            <center className="pages">
                                <div className="navigate" onClick={() => this.back()}><i className="fas fa-angle-left"></i></div>
                                <label>{this.state.curPage + 1}</label>
                                <div className="navigate" onClick={() => this.next()}><i className="fas fa-angle-right"></i></div>
                            </center>
                        </div>
                    )}
                    
                </div>
                <ReactTooltip place="left"/>
            </div>
        )
    }
}


module.exports = App