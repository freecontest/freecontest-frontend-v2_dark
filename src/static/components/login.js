import React, { Component } from 'react'
import Navbar from '../components/navbar'
import Load from '../components/loader'
import {FreeContest as FC, history, toast } from '../js/action'

const wait = async (ms) => new Promise(res => setTimeout(res, ms))

class App extends Component{

    constructor(props){
        super(props);
        this.state={
            // alert : ""
            Logining: false
        }
        this.handleLogin = this.handleLogin.bind(this)
    }

    focus(e){
        let par_e = e.target.parentNode
        par_e.childNodes.forEach(e => e.classList.add('field-focus'))
    }

    blur(e){
        let par_e = e.target.parentNode
        par_e.childNodes.forEach(e => e.classList.remove('field-focus'))
    }

    async handleLogin(e){
        e.preventDefault()
        const target = e.target
        //0 is username, 1 is password
        let username = target[0].value,
            password = target[1].value
        if(username == "" || password == ""){
            if(username == ""){
                target[0].focus();
            }
            if(password == ""){
                target[1].focus();
            }

            toast.error('Thông báo', 'Bạn phải nhập đầy đủ thông tin!')
        }
        else{
            this.setState({ Logining: true })
            console.log(username, password)
            await FC.signIn(username, password, async (res) => {
                console.log(res)
                if(res.logined){
                    toast.success('Thông báo', 'Đăng nhập thành công')
                    this.setState({ Logining: false })
                    document.getElementsByClassName('logo')[0].style.fill = "rgb(9, 15, 29)"
                    document.getElementsByClassName('logo')[1].style.fill = "green"
                    await wait(2000)
                    history.push('/')
                }
                if(res.err != undefined){
                    toast.error('Thông báo', 'Không tồn tại tài khoản hoặc sai mật khẩu')
                    this.setState({ Logining: false })
                    document.getElementsByClassName('logo')[0].style.fill = "rgb(9, 15, 29)"
                    document.getElementsByClassName('logo')[1].style.fill = "red"
                }
            })
            
        }
    }

    componentDidMount(){
        document.title = "Đăng nhập"

        FC.getSession((res) => {
            if(res.logined){
                toast.error('Lỗi', 'Bạn phải đăng xuất trước')
                history.push('/')
            }
        })
    }

    render(){
        return (
            <div>
                <Navbar/>
                <div className="login">
                    {this.state.Logining ? (
                        <div className="loading">
                            <Load/>
                        </div>
                    ):(
                        <svg viewBox="0 0 600 600">
                            <path className="logo" d="M302.59,40.38,76.17,172.58,77,435.32l225.6,131.37L529,435.32V171.75Zm0,498.87L100.39,422.13l.13-237.08L302.59,67l202.2,118.06.68,237Z"/>
                            <path className="logo" d="M408.89,347.31a28.36,28.36,0,0,0-22.61-5.74l-39-80.81a55.58,55.58,0,0,0,24.33-46.19c0-30.54-24.17-55.29-54-55.29s-54,24.75-54,55.29c0,18.83,8.69,35.33,22.73,45.31L235.43,365.07a39.76,39.76,0,0,0-9.94-1.26c-22.41,0-40.57,18.62-40.57,41.57S203.08,447,225.49,447s40.58-18.62,40.58-41.58a41.84,41.84,0,0,0-15.36-32.55l50.37-105.62a52.89,52.89,0,0,0,32.38.22l38.71,81a28.84,28.84,0,0,0-4,4.33,30.2,30.2,0,0,0,5.3,41.69A28.54,28.54,0,0,0,414.19,389,30.2,30.2,0,0,0,408.89,347.31Z"/>
                        </svg>
                    )}
                    
                    <div className="login-form middle">
                        <h2>Đăng nhập</h2>
                        <hr></hr>
                        <form className="form-group col-sm-12" onSubmit={this.handleLogin}>
                            <div className="col-sm-12">
                                <div className="col-sm-12">
                                    <label className="field-name"> Tên tài khoản </label>
                                    <input className="field-input" name="username" type="text" placeholder="VD: pythagore1123" onFocus={(e) => this.focus(e)} onBlur={(e) => this.blur(e)}></input>
                                </div>
                                <div className="col-sm-12">
                                    <label className="field-name"> Mật khẩu</label>
                                    <input className="field-input" name="password" type="password" placeholder="●●●●●●●●●●●" onFocus={(e) => this.focus(e)} onBlur={(e) => this.blur(e)}></input>
                                </div>
                                    <button className="btn btn-primary col-sm-12" type="submit" disabled={this.state.Logining}>Đăng nhập</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

class Alert extends Component{

    messages(stat){
        switch(stat){
            case 'logined': return 'Đăng nhập thành công!'
            break
            case 'error': return 'Tên tài khoản hoặc mật khẩu không chính xác'
            break
            case 'missing' : return 'Bạn phải nhập đầy đủ thông tin!'
            break;
        }
    }

    class(stat){
        if(stat == "logined"){ return 'ok' }
        else { return 'error' }
    }

    render(){
        return(
            <div className={'info ' + this.class(this.props.stat)}>
                {this.messages(this.props.stat)}
            </div>
        )
    }
}

module.exports = App